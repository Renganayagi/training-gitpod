# Welcome

Hello! This `gitpod` environemnt has been set up in order to ease onboarding to Tezos smart contracts written 
in [Michelson](https://tezos.gitlab.io/008/michelson.html) and [Ligo](). 

It provides everything you need to start experimenting with the example, and solving the training exercises:
 
 - the Tezos client,
 - the Ligo compiler,
 - the Ligo and Michelson IDE plugins and syntax extensions,
 - a Michelson debugger.

