# Verifications : tests

Add tests to check that the contract from the previous exercise (verifications_1), works well.

In particular:
- check that the same person can’t subtract twice in a row
- check that only the owner may reset the counter
