# Exercise : call a contract 2

You should have been given the address of a contract for this task.

Start by setting an alias for this contract.

Then, call that smart contract, by providing your a number as a parameter.

Then, go check the contract on https://better-call.dev/
