# Exercice : appeler un contrat

On a dû vous donner l'adresse d'un contrat pour cet exercice.

Commencez par définir un alias vers ce contrat.

Appelez ce contract en donnant un nombre comme paramètre.

Allez ensuite voir le résultat sur https://better-call.dev/
